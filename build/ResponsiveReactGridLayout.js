"use strict";

exports.__esModule = true;

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lodash = require("lodash.isequal");

var _lodash2 = _interopRequireDefault(_lodash);

var _utils = require("./utils");

var _responsiveUtils = require("./responsiveUtils");

var _ReactGridLayout = require("./ReactGridLayout");

var _ReactGridLayout2 = _interopRequireDefault(_ReactGridLayout);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
}

var type = function type(obj) {
  return Object.prototype.toString.call(obj);
};

var ResponsiveReactGridLayout = function ResponsiveReactGridLayout(_ref) {
  var breakpoint = _ref.breakpoint,
    breakpoints = _ref.breakpoints,
    cols = _ref.cols,
    layouts = _ref.layouts,
    width = _ref.width,
    onBreakpointChange = _ref.onBreakpointChange,
    onLayoutChange = _ref.onLayoutChange,
    onWidthChange = _ref.onWidthChange,
    rest = _objectWithoutProperties(_ref, [
      "breakpoint",
      "breakpoints",
      "cols",
      "layouts",
      "width",
      "onBreakpointChange",
      "onLayoutChange",
      "onWidthChange"
    ]);

  // State
  var initialBreakpoint = (0, _responsiveUtils.getBreakpointFromWidth)(
    breakpoints,
    width
  );
  var initialCols = (0, _responsiveUtils.getColsFromBreakpoint)(
    initialBreakpoint,
    cols
  );

  var compactType = rest.verticalCompact === false ? null : rest.compactType;

  var _useState = (0, _react.useState)(function() {
      return (0,
      _responsiveUtils.findOrGenerateResponsiveLayout)(layouts, breakpoints, initialBreakpoint, initialBreakpoint, initialCols, compactType);
    }),
    localLayout = _useState[0],
    setLocalLayout = _useState[1];

  var _useState2 = (0, _react.useState)(function() {
      return initialBreakpoint;
    }),
    localBreakpoint = _useState2[0],
    setLocalBreakpoints = _useState2[1];

  var _useState3 = (0, _react.useState)(function() {
      return initialCols;
    }),
    localCols = _useState3[0],
    setLocalCols = _useState3[1];

  // wrap layouts so we do not need to pass layouts to child

  var onLayoutChangeHandle = function onLayoutChangeHandle(layout) {
    var _extends2;

    onLayoutChange(
      layout,
      _extends(
        {},
        layouts,
        ((_extends2 = {}), (_extends2[localBreakpoint] = layout), _extends2)
      )
    );
  };

  /**
   * When the width changes work through breakpoints and reset state with the new width & breakpoint.
   * Width changes are necessary to figure out the widget widths.
   */
  var onWidthChangeHandle = function onWidthChangeHandle() {
    var newBreakpoint =
      breakpoint ||
      (0, _responsiveUtils.getBreakpointFromWidth)(breakpoints, width);

    var lastBreakpoint = localBreakpoint;
    var newCols = (0, _responsiveUtils.getColsFromBreakpoint)(
      newBreakpoint,
      cols
    );

    // Breakpoint change
    if (lastBreakpoint !== newBreakpoint || localCols !== cols) {
      // Preserve the current layout if the current breakpoint is not present in the next layouts.
      if (!(lastBreakpoint in layouts))
        layouts[lastBreakpoint] = (0, _utils.cloneLayout)(localLayout);

      // Find or generate a new layout.
      var layout = (0, _responsiveUtils.findOrGenerateResponsiveLayout)(
        layouts,
        breakpoints,
        newBreakpoint,
        lastBreakpoint,
        newCols,
        compactType
      );

      // This adds missing items.
      layout = (0, _utils.synchronizeLayoutWithChildren)(
        layout,
        rest.children,
        newCols,
        compactType
      );

      // Store the new layout.
      layouts[newBreakpoint] = layout;

      // callbacks
      onLayoutChange(layout, layouts);
      onBreakpointChange(newBreakpoint, newCols);

      setLocalLayout(layout);
      setLocalBreakpoints(newBreakpoint);
      setLocalCols(newCols);
    }
    //call onWidthChange on every change of width, not only on breakpoint changes
    onWidthChange(width, rest.margin, newCols, rest.containerPadding);
  };

  (0, _react.useEffect)(
    function() {
      // Since we're setting an entirely new layout object, we must generate a new responsive layout
      // if one does not exist.
      var newLayout = (0, _responsiveUtils.findOrGenerateResponsiveLayout)(
        layouts,
        breakpoints,
        localBreakpoint,
        localBreakpoint,
        cols[localBreakpoint],
        compactType
      );

      setLocalLayout(newLayout);
    },
    [layouts, breakpoint, cols]
  );

  (0, _react.useEffect)(
    function() {
      onWidthChangeHandle();
    },
    [width]
  );

  return _react2.default.createElement(
    _ReactGridLayout2.default,
    _extends({}, rest, {
      width: width,
      onLayoutChange: onLayoutChangeHandle,
      layout: layouts[localBreakpoint],
      cols: localCols
    })
  );
};

ResponsiveReactGridLayout.defaultProps = {
  breakpoints: { lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 },
  cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
  layouts: {},
  onBreakpointChange: _utils.noop,
  onLayoutChange: _utils.noop,
  onWidthChange: _utils.noop
};

// This should only include propTypes needed in this code; RGL itself
// will do validation of the rest props passed to it.
ResponsiveReactGridLayout.propTypes = {
  //
  // Basic props
  //

  // Optional, but if you are managing width yourself you may want to set the breakpoint
  // yourself as well.
  breakpoint: _propTypes2.default.string,

  // {name: pxVal}, e.g. {lg: 1200, md: 996, sm: 768, xs: 480}
  breakpoints: _propTypes2.default.object,

  // # of cols. This is a breakpoint -> cols map
  cols: _propTypes2.default.object,

  // layouts is an object mapping breakpoints to layouts.
  // e.g. {lg: Layout, md: Layout, ...}
  layouts: function layouts(props, propName) {
    if (type(props[propName]) !== "[object Object]") {
      throw new Error(
        "Layout property must be an object. Received: " + type(props[propName])
      );
    }
    Object.keys(props[propName]).forEach(function(key) {
      if (!(key in props.breakpoints)) {
        throw new Error(
          "Each key in layouts must align with a key in breakpoints."
        );
      }
      (0, _utils.validateLayout)(props.layouts[key], "layouts." + key);
    });
  },

  // The width of this component.
  // Required in this propTypes stanza because generateInitialState() will fail without it.
  width: _propTypes2.default.number.isRequired,

  //
  // Callbacks
  //

  // Calls back with breakpoint and new # cols
  onBreakpointChange: _propTypes2.default.func,

  // Callback so you can save the layout.
  // Calls back with (currentLayout, allLayouts). allLayouts are keyed by breakpoint.
  onLayoutChange: _propTypes2.default.func,

  // Calls back with (containerWidth, margin, cols, containerPadding)
  onWidthChange: _propTypes2.default.func
};

// $FlowFixMe
var arePropsEqual = function arePropsEqual(prevProps, nextProps) {
  return (
    (0, _utils.childrenEqual)(prevProps.children, nextProps.children) &&
    (0, _lodash2.default)(nextProps.breakpoints, prevProps.breakpoints) &&
    (0, _lodash2.default)(nextProps.cols, prevProps.cols) &&
    prevProps.width === nextProps.width &&
    (0, _lodash2.default)(nextProps.layouts, prevProps.layouts) &&
    prevProps.compactType === nextProps.compactType
  );
};

// $FlowFixMe
exports.default = (0, _react.memo)(ResponsiveReactGridLayout, arePropsEqual);
