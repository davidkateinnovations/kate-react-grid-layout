"use strict";

exports.__esModule = true;

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDraggable = require("react-draggable");

var _reactResizable = require("react-resizable");

var _utils = require("./utils");

var _classnames = require("classnames");

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * An individual item within a ReactGridLayout.
 */

// State
var GridItem = function GridItem(_ref) {
  var children = _ref.children,
    cols = _ref.cols,
    containerWidth = _ref.containerWidth,
    margin = _ref.margin,
    containerPadding = _ref.containerPadding,
    rowHeight = _ref.rowHeight,
    maxRows = _ref.maxRows,
    isDraggable = _ref.isDraggable,
    isResizable = _ref.isResizable,
    isStatic = _ref.isStatic,
    useCSSTransforms = _ref.useCSSTransforms,
    usePercentages = _ref.usePercentages,
    className = _ref.className,
    style = _ref.style,
    cancel = _ref.cancel,
    handle = _ref.handle,
    x = _ref.x,
    y = _ref.y,
    w = _ref.w,
    h = _ref.h,
    minW = _ref.minW,
    maxW = _ref.maxW,
    minH = _ref.minH,
    maxH = _ref.maxH,
    i = _ref.i,
    onDrag = _ref.onDrag,
    onDragStart = _ref.onDragStart,
    onDragStop = _ref.onDragStop,
    onResize = _ref.onResize,
    onResizeStart = _ref.onResizeStart,
    onResizeStop = _ref.onResizeStop;

  var handlers = {
    onDrag: onDrag,
    onDragStart: onDragStart,
    onDragStop: onDragStop,
    onResize: onResize,
    onResizeStart: onResizeStart,
    onResizeStop: onResizeStop
  };

  // State

  var _useState = (0, _react.useState)(function() {
      return null;
    }),
    resizing = _useState[0],
    setResizing = _useState[1];

  var _useState2 = (0, _react.useState)(function() {
      return null;
    }),
    dragging = _useState2[0],
    setDragging = _useState2[1];

  // Helper for generating column width

  var calcColWidth = function calcColWidth() {
    return (
      (containerWidth - margin[0] * (cols - 1) - containerPadding[0] * 2) / cols
    );
  };

  /**
   * Return position on the page given an x, y, w, h.
   * left, top, width, height are all in pixels.
   * @param  {Number}  x             X coordinate in grid units.
   * @param  {Number}  y             Y coordinate in grid units.
   * @param  {Number}  w             W coordinate in grid units.
   * @param  {Number}  h             H coordinate in grid units.
   * @param  {Resizing}  resizing    Object with resizing params
   * @param  {Dragging}  dragging    Object with dragging params
   * @return {Object}                Object containing coords.
   */
  var calcPosition = function calcPosition(x, y, w, h, resizing, dragging) {
    var colWidth = calcColWidth();
    var out = {
      left: Math.round((colWidth + margin[0]) * x + containerPadding[0]),
      top: Math.round((rowHeight + margin[1]) * y + containerPadding[1]),
      // 0 * Infinity === NaN, which causes problems with resize constraints;
      // Fix this if it occurs.
      // Note we do it here rather than later because Math.round(Infinity) causes deopt
      width:
        w === Infinity
          ? w
          : Math.round(colWidth * w + Math.max(0, w - 1) * margin[0]),
      height:
        h === Infinity
          ? h
          : Math.round(rowHeight * h + Math.max(0, h - 1) * margin[1])
    };

    if (resizing) {
      out.width = Math.round(resizing.width);
      out.height = Math.round(resizing.height);
    }

    if (dragging) {
      out.top = Math.round(dragging.top);
      out.left = Math.round(dragging.left);
    }

    return out;
  };

  /**
   * Translate x and y coordinates from pixels to grid units.
   * @param  {Number} top  Top position (relative to parent) in pixels.
   * @param  {Number} left Left position (relative to parent) in pixels.
   * @return {Object} x and y in grid units.
   */
  var calcXY = function calcXY(top, left) {
    var colWidth = calcColWidth();

    // left = colWidth * x + margin * (x + 1)
    // l = cx + m(x+1)
    // l = cx + mx + m
    // l - m = cx + mx
    // l - m = x(c + m)
    // (l - m) / (c + m) = x
    // x = (left - margin) / (coldWidth + margin)
    var x = Math.round((left - margin[0]) / (colWidth + margin[0]));
    var y = Math.round((top - margin[1]) / (rowHeight + margin[1]));

    // Capping
    x = Math.max(Math.min(x, cols - w), 0);
    y = Math.max(Math.min(y, maxRows - h), 0);

    return { x: x, y: y };
  };

  /**
   * Given a height and width in pixel values, calculate grid units.
   * @param  {Number} height Height in pixels.
   * @param  {Number} width  Width in pixels.
   * @return {Object} w, h as grid units.
   */
  var calcWH = function calcWH(_ref2) {
    var height = _ref2.height,
      width = _ref2.width;

    var colWidth = calcColWidth();

    // width = colWidth * w - (margin * (w - 1))
    // ...
    // w = (width + margin) / (colWidth + margin)
    var w = Math.round((width + margin[0]) / (colWidth + margin[0]));
    var h = Math.round((height + margin[1]) / (rowHeight + margin[1]));

    // Capping
    w = Math.max(Math.min(w, cols - x), 0);
    h = Math.max(Math.min(h, maxRows - y), 0);
    return { w: w, h: h };
  };

  /**
   * This is where we set the grid item's absolute placement. It gets a little tricky because we want to do it
   * well when server rendering, and the only way to do that properly is to use percentage width/left because
   * we don't know exactly what the browser viewport is.
   * Unfortunately, CSS Transforms, which are great for performance, break in this instance because a percentage
   * left is relative to the item itself, not its container! So we cannot use them on the server rendering pass.
   *
   * @param  {Object} pos Position object with width, height, left, top.
   * @return {Object}     Style object.
   */
  var createStyle = function createStyle(pos) {
    var style = void 0;
    // CSS Transforms support (default)
    if (useCSSTransforms) {
      style = (0, _utils.setTransform)(pos);
    } else {
      // top,left (slow)
      style = (0, _utils.setTopLeft)(pos);

      // This is used for server rendering.
      if (usePercentages) {
        style.left = (0, _utils.perc)(pos.left / containerWidth);
        style.width = (0, _utils.perc)(pos.width / containerWidth);
      }
    }

    return style;
  };

  /**
   * Wrapper around drag events to provide more useful data.
   * All drag events call the function with the given handler name,
   * with the signature (index, x, y).
   *
   * @param  {String} handlerName Handler name to wrap.
   * @return {Function}           Handler function.
   */
  var onResizeHandler = function onResizeHandler(handlerName) {
    return function(e, _ref3) {
      var node = _ref3.node,
        size = _ref3.size;

      var handler = handlers[handlerName];
      if (!handler) return;

      // Get new XY

      var _calcWH = calcWH(size),
        w = _calcWH.w,
        h = _calcWH.h;

      // Cap w at numCols

      w = Math.min(w, cols - x);
      // Ensure w is at least 1
      w = Math.max(w, 1);

      // Min/max capping
      w = Math.max(Math.min(w, maxW), minW);
      h = Math.max(Math.min(h, maxH), minH);

      setResizing(handlerName === "onResizeStop" ? null : size);
      handler(i, w, h, { e: e, node: node, size: size });
    };
  };

  /**
   * Wrapper around drag events to provide more useful data.
   * All drag events call the function with the given handler name,
   * with the signature (index, x, y).
   *
   * @param  {String} handlerName Handler name to wrap.
   * @return {Function}           Handler function.
   */
  var onDragHandler = function onDragHandler(handlerName) {
    return function(e, _ref4) {
      var node = _ref4.node,
        deltaX = _ref4.deltaX,
        deltaY = _ref4.deltaY;

      var handler = handlers[handlerName];
      if (!handler) return;

      var newPosition = { top: 0, left: 0 };

      // Get new XY
      switch (handlerName) {
        case "onDragStart": {
          // TODO: this wont work on nested parents
          var offsetParent = node.offsetParent;

          if (!offsetParent) return;
          var parentRect = offsetParent.getBoundingClientRect();
          var clientRect = node.getBoundingClientRect();
          newPosition.left =
            clientRect.left - parentRect.left + offsetParent.scrollLeft;
          newPosition.top =
            clientRect.top - parentRect.top + offsetParent.scrollTop;
          setDragging(newPosition);
          break;
        }
        case "onDrag":
          if (!dragging) throw new Error("onDrag called before onDragStart.");
          newPosition.left = dragging.left + deltaX;
          newPosition.top = dragging.top + deltaY;
          setDragging(newPosition);
          break;
        case "onDragStop":
          if (!dragging)
            throw new Error("onDragEnd called before onDragStart.");
          newPosition.left = dragging.left;
          newPosition.top = dragging.top;
          setDragging(null);
          break;
        default:
          throw new Error(
            "onDragHandler called with unrecognized handlerName: " + handlerName
          );
      }

      var _calcXY = calcXY(newPosition.top, newPosition.left),
        x = _calcXY.x,
        y = _calcXY.y;

      return handler(i, x, y, { e: e, node: node, newPosition: newPosition });
    };
  };

  /**
   * Mix a Draggable instance into a child.
   * @param  {Element} child    Child element.
   * @return {Element}          Child wrapped in Draggable.
   */
  var mixinDraggable = function mixinDraggable(child) {
    return _react2.default.createElement(
      _reactDraggable.DraggableCore,
      {
        onStart: onDragHandler("onDragStart"),
        onDrag: onDragHandler("onDrag"),
        onStop: onDragHandler("onDragStop"),
        handle: handle,
        cancel: ".react-resizable-handle" + (cancel ? "," + cancel : "")
      },
      child
    );
  };

  /**
   * Mix a Resizable instance into a child.
   * @param  {Element} child    Child element.
   * @param  {Object} position  Position object (pixel values)
   * @return {Element}          Child wrapped in Resizable.
   */
  var mixinResizable = function mixinResizable(child, position) {
    // This is the max possible width - doesn't go to infinity because of the width of the window
    var maxWidth = calcPosition(0, 0, cols - x, 0).width;

    // Calculate min/max constraints using our min & maxes
    var mins = calcPosition(0, 0, minW, minH);
    var maxes = calcPosition(0, 0, maxW, maxH);
    var minConstraints = [mins.width, mins.height];
    var maxConstraints = [
      Math.min(maxes.width, maxWidth),
      Math.min(maxes.height, Infinity)
    ];
    return _react2.default.createElement(
      _reactResizable.Resizable,
      {
        width: position.width,
        height: position.height,
        minConstraints: minConstraints,
        maxConstraints: maxConstraints,
        onResizeStop: onResizeHandler("onResizeStop"),
        onResizeStart: onResizeHandler("onResizeStart"),
        onResize: onResizeHandler("onResize")
      },
      child
    );
  };

  var pos = calcPosition(x, y, w, h, resizing, dragging);
  var child = _react2.default.Children.only(children);

  // Create the child element. We clone the existing element but modify its className and style.
  var newChild = _react2.default.cloneElement(child, {
    className: (0, _classnames2.default)(
      "react-grid-item",
      child.props.className,
      className,
      {
        static: isStatic,
        resizing: Boolean(resizing),
        "react-draggable": isDraggable,
        "react-draggable-dragging": Boolean(dragging),
        cssTransforms: useCSSTransforms
      }
    ),
    // We can set the width and height on the child, but unfortunately we can't set the position.
    style: _extends({}, style, child.props.style, createStyle(pos))
  });

  // Resizable support. This is usually on but the user can toggle it off.
  if (isResizable) newChild = mixinResizable(newChild, pos);

  // Draggable support. This is always on, except for with placeholders.
  if (isDraggable) newChild = mixinDraggable(newChild);

  return newChild;
};

GridItem.defaultProps = {
  className: "",
  cancel: "",
  handle: "",
  minH: 1,
  minW: 1,
  maxH: Infinity,
  maxW: Infinity
};

GridItem.propTypes = {
  // Children must be only a single element
  children: _propTypes2.default.element,

  // General grid attributes
  cols: _propTypes2.default.number.isRequired,
  containerWidth: _propTypes2.default.number.isRequired,
  rowHeight: _propTypes2.default.number.isRequired,
  margin: _propTypes2.default.array.isRequired,
  maxRows: _propTypes2.default.number.isRequired,
  containerPadding: _propTypes2.default.array.isRequired,

  // These are all in grid units
  x: _propTypes2.default.number.isRequired,
  y: _propTypes2.default.number.isRequired,
  w: _propTypes2.default.number.isRequired,
  h: _propTypes2.default.number.isRequired,

  // All optional
  minW: function minW(props, propName) {
    var value = props[propName];
    if (typeof value !== "number") return new Error("minWidth not Number");
    if (value > props.w || value > props.maxW)
      return new Error("minWidth larger than item width/maxWidth");
  },

  maxW: function maxW(props, propName) {
    var value = props[propName];
    if (typeof value !== "number") return new Error("maxWidth not Number");
    if (value < props.w || value < props.minW)
      return new Error("maxWidth smaller than item width/minWidth");
  },

  minH: function minH(props, propName) {
    var value = props[propName];
    if (typeof value !== "number") return new Error("minHeight not Number");
    if (value > props.h || value > props.maxH)
      return new Error("minHeight larger than item height/maxHeight");
  },

  maxH: function maxH(props, propName) {
    var value = props[propName];
    if (typeof value !== "number") return new Error("maxHeight not Number");
    if (value < props.h || value < props.minH)
      return new Error("maxHeight smaller than item height/minHeight");
  },

  // ID is nice to have for callbacks
  i: _propTypes2.default.string.isRequired,

  // Functions
  onDragStop: _propTypes2.default.func,
  onDragStart: _propTypes2.default.func,
  onDrag: _propTypes2.default.func,
  onResizeStop: _propTypes2.default.func,
  onResizeStart: _propTypes2.default.func,
  onResize: _propTypes2.default.func,

  // Flags
  isDraggable: _propTypes2.default.bool.isRequired,
  isResizable: _propTypes2.default.bool.isRequired,
  isStatic: _propTypes2.default.bool,

  // Use CSS transforms instead of top/left
  useCSSTransforms: _propTypes2.default.bool.isRequired,

  // Others
  className: _propTypes2.default.string,
  // Selector for draggable handle
  handle: _propTypes2.default.string,
  // Selector for draggable cancel (see react-draggable)
  cancel: _propTypes2.default.string
};

exports.default = GridItem;
