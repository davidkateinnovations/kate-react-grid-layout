"use strict";

exports.__esModule = true;

var _extends =
  Object.assign ||
  function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function _objectWithoutProperties(obj, keys) {
  var target = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
}

/*
 * A simple HOC that provides facility for listening to container resizes.
 */
function WidthProvider(ComposedComponent) {
  var WidthProvider = function WidthProvider(_ref) {
    var className = _ref.className,
      measureBeforeMount = _ref.measureBeforeMount,
      style = _ref.style,
      rest = _objectWithoutProperties(_ref, [
        "className",
        "measureBeforeMount",
        "style"
      ]);

    var currentRef = (0, _react.useRef)({ current: null });

    var _useState = (0, _react.useState)(function() {
        return 1280;
      }),
      width = _useState[0],
      setWidth = _useState[1];

    var _useState2 = (0, _react.useState)(function() {
        return false;
      }),
      mounted = _useState2[0],
      setMounted = _useState2[1];

    var onWindowResize = function onWindowResize() {
      if (!mounted) return;
      if (!currentRef || !currentRef.current || !currentRef.current.clientWidth)
        return;
      setWidth(currentRef.current.clientWidth);
    };

    (0, _react.useEffect)(function() {
      setMounted(true);

      window.addEventListener("resize", onWindowResize);
      // Call to properly set the breakpoint and resize the elements.
      // Note that if you're doing a full-width element, this can get a little wonky if a scrollbar
      // appears because of the grid. In that case, fire your own resize event, or set `overflow: scroll` on your body.
      onWindowResize();
      return function() {
        setMounted(false);
        window.removeEventListener("resize", onWindowResize);
      };
    });

    if (measureBeforeMount && !mounted) {
      return _react2.default.createElement("div", {
        className: className,
        style: style
      });
    }

    return (
      // $FlowFixMe
      _react2.default.createElement(
        "div",
        { ref: currentRef },
        _react2.default.createElement(
          ComposedComponent,
          _extends({}, rest, { width: width })
        )
      )
    );
  };

  WidthProvider.defaultProps = {
    measureBeforeMount: false
  };

  WidthProvider.propTypes = {
    // If true, will not render children until mounted. Useful for getting the exact width before
    // rendering, to prevent any unsightly resizing.
    measureBeforeMount: _propTypes2.default.bool
  };

  return WidthProvider;
}

exports.default = WidthProvider;
